import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:kooch/Views/Categories/categories.dart' as ct;
import 'package:kooch/Views/Coap/coap_form.dart';
import 'package:kooch/Views/ComingSoon/coming_soon.dart';
import 'package:kooch/Views/Home/home.dart';
import 'package:kooch/Views/MainPage/main_page.dart';
import 'package:kooch/Views/Profile/profile.dart';
import 'package:kooch/Views/Ticket/ticket.dart';

class GetChangePageController extends GetxController {
  final List<Widget> dialogs = [];

  final List<Widget> pages = [
    const Home(),
    const Profile(),
    const CoapForm(),
    const ct.Categories(),
    const Ticket(),
    const ComingSoon(),
  ];

  int _lastPageIndex = 0;
  final RxInt _dialogIndex = 0.obs;
  final RxInt _mainPageIndex = 0.obs;
  final RxString _currentCatID = "0".obs;
  final RxString _currentSubID = "0".obs;

  final RxBool _shouldShowProfile = true.obs;
  final RxBool _shouldScroll = true.obs;
  final RxBool _showAskingForm = false.obs;
  final RxInt _askingFormLevel = 0.obs;
  final RxBool _coapItemShowed = false.obs;
  final RxInt _categoriesCurrentPage = 0.obs;

  PageController? levelController;

  bool get coapItemShowed => _coapItemShowed.value;
  bool get showAskingForm => _showAskingForm.value;

  int get mainPageIndex => _mainPageIndex.value;
  int get lastPageIndex => _lastPageIndex;
  int get askingFormLevel => _askingFormLevel.value;
  int get dialogIndex => _dialogIndex.value;
  String get currentCatID => _currentCatID.value;
  int get categoriesCurrentPage => _categoriesCurrentPage.value;
  String get currentSubID => _currentSubID.value;

  bool get shouldScroll => _shouldScroll.value;
  bool get shouldShowProfile => _shouldShowProfile.value;

  set coapItemShowed(bool myValue) {
    _coapItemShowed.value = myValue;
  }

  set showAskingForm(bool myValue) {
    _showAskingForm.value = myValue;
  }

  set shouldShowProfile(bool myValue) {
    _shouldShowProfile.value = myValue;
  }

  set currentCatID(String value) {
    _currentCatID.value = value;
  }

  set mainPageIndex(int myValue) {
    _mainPageIndex.value = myValue;
  }

  set dialogIndex(int myValue) {
    _dialogIndex.value = myValue;
  }

  set askingFormLevel(int myValue) {
    _askingFormLevel.value = myValue;
  }

  set shouldScroll(bool myValue) {
    _shouldScroll.value = myValue;
  }

  set categoriesCurrentPage(int myValue) {
    _categoriesCurrentPage.value = myValue;
  }

  set currentSubID(String myValue) {
    _currentSubID.value = myValue;
  }

  Future changePage(int index,
      {bool shouldScroll = true, bool isBack = false}) async {
    FocusScopeNode currentFocus = FocusScope.of(Get.context!);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.focusedChild?.unfocus();
    }
    if (isBack) {
      if (mainPageIndex == 0) {
        if (Get.currentRoute == "/Categories") {
          Get.offAll(() => MainPage());

          return;
        }

        await Get.showSnackbar(GetBar(
          message: 'برای خروج اینجا ضربه بزنید',
          duration: const Duration(milliseconds: 1500),
          onTap: (snack) {
            exit(0);
          },
        ));
        return;
      }

      if (index == mainPageIndex) {
        mainPageIndex = 0;
        return;
      }
    }

    if (index == mainPageIndex) {
      return;
    }
    if (!isBack) {
      _lastPageIndex = mainPageIndex;
    }
    mainPageIndex = index;

    if (mainPageIndex == 0) {
      shouldShowProfile = true;
    } else {
      shouldShowProfile = false;
    }
    this.shouldScroll = shouldScroll;
  }

  backIconCallBack() {
    if (Get.currentRoute == "/Categories" && categoriesCurrentPage > 0) {
      categoriesCurrentPage--;
      return;
    }
    if (mainPageIndex == 2) {
      if (coapItemShowed) {
        coapItemShowed = false;

        return;
      } else if (showAskingForm && askingFormLevel == 1) {
        showAskingForm = false;

        return;
      } else if (showAskingForm && askingFormLevel > 1) {
        _askingFormLevel.value--;
        levelController?.animateToPage(askingFormLevel - 1,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 400));
        return;
      }
    }
    changePage(_lastPageIndex,
        shouldScroll: mainPageIndex == 2 ? false : true, isBack: true);
  }

  nextAskingFormLevel() {
    if (_askingFormLevel > 5) return;
    _askingFormLevel.value++;
    levelController?.animateToPage(askingFormLevel - 1,
        curve: Curves.easeOut, duration: const Duration(milliseconds: 400));
  }

  askDone() async {
    await changePage(
      0,
      shouldScroll: true,
    );
    await Future.delayed(const Duration(milliseconds: 500));
    askingFormLevel = 1;
    showAskingForm = false;

    // navigator?.push(FadeInTransition(opaque: true, widget: MainPage()));
  }

  reset() {
    _lastPageIndex = 0;

    dialogIndex = 0;
    mainPageIndex = 0;
    currentCatID = "0";
    currentSubID = "0";

    shouldShowProfile = true;
    shouldScroll = true;
    showAskingForm = false;
    askingFormLevel = 1;
    coapItemShowed = false;
    categoriesCurrentPage = 0;
  }
}
