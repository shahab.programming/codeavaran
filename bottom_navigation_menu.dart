import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kooch/GetxControllers/get_change_page_controller.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:kooch/Views/BottomNavigationMenu/nav_button.dart';
import 'package:kooch/Views/Coap/coap_form.dart';
import 'package:kooch/Views/MainPage/main_page.dart';
import 'package:kooch/Views/PageRouteTransitions/slide_in_transition.dart';
import 'package:kooch/Views/Widgets/keyboard_visibility_builder.dart';
import 'package:kooch/constants.dart';
import 'bottom_navigation_menu_paint.dart';

class BottomNavigationMenu extends StatelessWidget {
  final bool isMain;

  const BottomNavigationMenu({required this.isMain});

  @override
  Widget build(BuildContext context) {
    final GetChangePageController changePageController = Get.find();

    return KeyboardVisibilityBuilder(
      builder: (BuildContext context, Widget child, bool isKeyboardVisible) {
        if (isKeyboardVisible) {
          return const SizedBox.shrink();
        } else {
          return child;
        }
      },
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(left: 0, bottom: 0, child: BottomNavigationMenuPaint()),
          Positioned(
              bottom: 5,
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                      child: Obx(
                    () => AnimatedPadding(
                      padding: EdgeInsets.only(
                          bottom:
                              changePageController.mainPageIndex == 1 ? 10 : 0),
                      duration: const Duration(milliseconds: 300),
                      child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 300),
                        opacity:
                            changePageController.mainPageIndex == 1 ? 1 : 0,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.purple, shape: BoxShape.circle),
                          height: 6,
                          width: 6,
                        ),
                      ),
                    ),
                  )),
                  Expanded(
                      child: Obx(
                    () => AnimatedPadding(
                      padding: EdgeInsets.only(
                          bottom:
                              changePageController.mainPageIndex == 4 ? 10 : 0),
                      duration: const Duration(milliseconds: 300),
                      child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 300),
                        opacity:
                            changePageController.mainPageIndex == 4 ? 1 : 0,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.purple, shape: BoxShape.circle),
                          height: 6,
                          width: 6,
                        ),
                      ),
                    ),
                  )),
                  const Spacer(),
                  Expanded(
                      child: Obx(
                    () => AnimatedPadding(
                      padding: EdgeInsets.only(
                          bottom:
                              changePageController.mainPageIndex == 2 ? 10 : 0),
                      duration: const Duration(milliseconds: 300),
                      child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 300),
                        opacity:
                            changePageController.mainPageIndex == 2 ? 1 : 0,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.purple, shape: BoxShape.circle),
                          height: 6,
                          width: 6,
                        ),
                      ),
                    ),
                  )),
                  Expanded(
                      child: Obx(
                    () => AnimatedPadding(
                      padding: EdgeInsets.only(
                          bottom:
                              changePageController.mainPageIndex == 5 ? 10 : 0),
                      duration: const Duration(milliseconds: 300),
                      child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 300),
                        opacity:
                            changePageController.mainPageIndex == 5 ? 1 : 0,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.purple, shape: BoxShape.circle),
                          height: 6,
                          width: 6,
                        ),
                      ),
                    ),
                  )),
                ],
              )),
          Positioned(
            bottom: 5,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  textDirection: TextDirection.ltr,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 0),
                        child: NavButton(
                          onTap: () => changePage(
                              5, changePageController, isMain,
                              shouldScroll: false),
                          index: 5,
                          image:
                              "${Constants.IMAGE_FOLDER}home_categoryoff.svg",
                        ),
                      ),
                    ),
                    Expanded(
                      child: NavButton(
                        onTap: () async {
                          if (Get.currentRoute == '/CoapForm') {
                            return;
                          }

                          await changePage(2, changePageController, isMain,
                              shouldScroll: false);
                          Get.to(() => CoapForm());
                        },
                        index: 2,
                        image: "${Constants.IMAGE_FOLDER}home_coapoff.svg",
                      ),
                    ),
                    const Spacer(),
                    Expanded(
                      child: NavButton(
                        index: 4,
                        onTap: () => changePage(4, changePageController, isMain,
                            shouldScroll: false),
                        image: "${Constants.IMAGE_FOLDER}home_ticket.svg",
                      ),
                    ),
                    Expanded(
                      child: NavButton(
                        onTap: () => changePage(1, changePageController, isMain,
                            shouldScroll: false),
                        index: 1,
                        image: "${Constants.IMAGE_FOLDER}home_profileoff.svg",
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          HomeButton(
            changePageController: changePageController,
            isMain: isMain,
          ),
        ],
      ),
    );
  }
}

changePage(int index, GetChangePageController changePageController, bool isMain,
    {bool shouldScroll = true}) {
  // return;
  changePageController
      .changePage(index, shouldScroll: shouldScroll)
      .then((value) {
    if (!isMain) navigator?.push(SlideInTransition(widget: MainPage()));
  });
}

class HomeButton extends StatelessWidget {
  final bool isMain;
  final GetChangePageController changePageController;
  const HomeButton(
      {Key? key, required this.isMain, required this.changePageController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: -0,
      left: 0,
      right: 0,
      child: GestureDetector(
        onTap: () => changePage(0, changePageController, isMain),
        child: Center(
          child: Container(
            height: 46,
            width: 46,
            decoration: BoxDecoration(
              color: Colors.black,
              shape: BoxShape.circle,
              gradient: const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [const Color(0xff4808C3), const Color(0xff14013E)],
                tileMode: TileMode.repeated,
              ),
            ),
            padding: const EdgeInsets.all(14),
            child: Center(
              child: SvgPicture.asset(
                "${Constants.IMAGE_FOLDER}home_homebtn.svg",
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
