import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kooch/GetxControllers/get_change_page_controller.dart';
import 'package:kooch/Views/BottomNavigationMenu/bottom_navigation_menu.dart';
import 'package:kooch/Views/Widgets/my_back_icon.dart';
import 'package:kooch/Views/Widgets/search_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../constants.dart';

class Body extends StatelessWidget {
  const Body();
  Widget build(BuildContext context) {
    final GetChangePageController changePageController = Get.find();
    final Size size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        changePageController.backIconCallBack();
        return false;
      },
      child: SafeArea(
        child: Stack(
          fit: StackFit.loose,
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Obx(
                () => AnimatedCrossFade(
                  crossFadeState: changePageController.mainPageIndex != 1
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  duration: const Duration(milliseconds: 300),
                  firstCurve: Curves.easeIn,
                  secondCurve: Curves.easeIn,
                  sizeCurve: Curves.easeIn,
                  reverseDuration: const Duration(milliseconds: 300),
                  firstChild: Image.asset(
                    "${Constants.IMAGE_FOLDER}TopAppBar.png",
                    width: size.width,
                    fit: BoxFit.fill,
                  ),
                  secondChild: Image.asset(
                    "${Constants.IMAGE_FOLDER}profileAppBar.png",
                    fit: BoxFit.fill,
                    width: size.width,
                    height: 199,
                  ),
                ),
              ),
            ),
            Positioned(
              width: size.width,
              child: BodyAppBar(
                changePageController: changePageController,
                size: size,
              ),
              top: 0,
            ),
            Obx(
              () => Positioned(
                  top: changePageController.mainPageIndex == 1 ? 0 : 75,
                  left: 0,
                  right: 0,
                  height: size.height,
                  child: Obx(() {
                    final int index = changePageController.mainPageIndex == 2
                        ? changePageController.lastPageIndex
                        : changePageController.mainPageIndex;
                    return ListView(
                      physics: changePageController.shouldScroll
                          ? null
                          : const NeverScrollableScrollPhysics(),
                      children: [
                        AnimatedSwitcher(
                          duration: const Duration(milliseconds: 300),
                          switchInCurve: Curves.easeIn,
                          switchOutCurve: Curves.easeOut,
                          reverseDuration: const Duration(milliseconds: 300),
                          child: changePageController.pages[index],
                        ),
                      ],
                    );
                  })),
            ),
            Obx(() {
              if (changePageController.mainPageIndex != 1)
                return AnimatedPositioned(
                  top: 24,
                  left: changePageController.shouldShowProfile ? 18 : 71,
                  right: changePageController.shouldShowProfile ? 71 : 18,
                  curve: Curves.easeIn,
                  height: 36,
                  duration: const Duration(milliseconds: 300),
                  child: SearchBar(),
                );
              return Container();
            }),
            Positioned(
              bottom: 0,
              height: 85,
              width: size.width,
              child: BottomNavigationMenu(
                isMain: true,
              ),
            ),
            Obx(() {
              if (changePageController.mainPageIndex == 1)
                return Positioned(
                  top: 24,
                  left: 18,
                  width: 36,
                  height: 36,
                  child: MyBackIcon(),
                );
              return Container();
            })
          ],
        ),
      ),
    );
  }
}

class BodyAppBar extends StatelessWidget {
  final GetChangePageController changePageController;
  final Size size;
  const BodyAppBar(
      {Key? key, required this.changePageController, required this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (changePageController.mainPageIndex == 1)
        return Container(
          height: 0,
          width: 0,
        );
      return AnimatedOpacity(
        duration: const Duration(milliseconds: 300),
        opacity: changePageController.mainPageIndex == 1 ? 0 : 1,
        child: Padding(
            padding: const EdgeInsets.only(right: 18, left: 18, top: 24),
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              width: size.width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AnimatedOpacity(
                    duration: const Duration(milliseconds: 300),
                    opacity: changePageController.mainPageIndex == 0 ? 1 : 0,
                    child: ProfileButton(),
                  ),
                  AnimatedOpacity(
                    duration: const Duration(milliseconds: 300),
                    opacity: changePageController.mainPageIndex != 0 ? 1 : 0,
                    child: MyBackIcon(),
                  ),
                ],
              ),
            )),
      );
    });
  }
}

class ProfileButton extends StatelessWidget {
  const ProfileButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        GetChangePageController getChangePageController = Get.find();
        getChangePageController.changePage(1);
      },
      child: Container(
        width: 36,
        height: 36,
        decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
          BoxShadow(
            blurRadius: 7,
            color: Color(0xff513F3F).withOpacity(.25),
            offset: Offset(0, 3),
          ),
        ]),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(18),
          child: FutureBuilder(
            future: SharedPreferences.getInstance(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                SharedPreferences prefs = snapshot.data;

                return prefs.getString('profileImage') == null
                    ? Image.asset(
                        "${Constants.IMAGE_FOLDER}Profile pic.png",
                        fit: BoxFit.fill,
                      )
                    : Image.network(
                        prefs.getString('profileImage')!,
                        fit: BoxFit.fill,
                        width: 72,
                        height: 72,
                      );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
